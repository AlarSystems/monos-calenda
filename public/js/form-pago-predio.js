$(document).ready(function(){
	$("#periodoFin").blur(function(){
		//alert("cambio");
		var periodoInicio = parseInt($("#periodoInicio").val());
		var periodoFin = parseInt($("#periodoFin").val());
		var cuenta_id = parseInt($('#cuenta_id').val());
		$.ajax({
			type:'POST',
			url:'/pago-predio/validarAnios',
			data: {
				periodoFin: periodoFin,
				periodoInicio: periodoInicio,
				cuenta_id: cuenta_id,
				"_token": $("#token").val() },
			dataType: "json"
		})
		.done(function(data){
			$("#montoActual").val(data.actual);
			$("#montoRezagos").val(data.rezago);
			if($('#montoActual').val() == 0){
				$('#descuentoActual').val(0);
				$('#descuentoActual').prop('disabled', true);
			}else{
				$('#descuentoActual').prop('disabled', false);
			}
			if($('#montoRezagos').val() == 0){
				$('#descuentoRezagos').val(0);
				$('#descuentoRezagos').prop('disabled', true);
			}else{
				$('#descuentoRezagos').prop('disabled', false);
			}
		})
		.fail(function(){
			console.log('Error en la peticion de datos');
		});
	});

});
