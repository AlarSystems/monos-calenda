$(document).ready(function(){
	$("#periodoFin").blur(function(){
		//alert("cambio");
		var periodoInicio = parseInt($("#periodoInicio").val());
		var periodoFin = parseInt($("#periodoFin").val());
		$.ajax({
			type:'POST',
			url:'/pago-espacio/validarAnios',
			data: {periodoFin: periodoFin, periodoInicio: periodoInicio, "_token": $("#token").val() },
			dataType: "json"
		})
		.done(function(data){
			$("#montoActual").val(data.actual);
			$("#montoRezagos").val(data.rezago);
			if($('#montoActual').val() == 0){
				$('#descuentoActual').val(0);
				$('#descuentoActual').prop('disabled', true);
			}else{
				$('#descuentoActual').prop('disabled', false);
			}
			if($('#montoRezagos').val() == 0){
				$('#descuentoRezagos').val(0);
				$('#descuentoRezagos').prop('disabled', true);
			}else{
				$('#descuentoRezagos').prop('disabled', false);
			}
		})
		.fail(function(){
			alert("Se murió");
		});
	});

	$("#total").focus(function(){
		//alert("Está funcionando esta madre");
		var actual = parseInt($("#montoActual").val());
		var rezago = parseInt($("#montoRezagos").val());
		var descActual = $("#descuentoActual").val()=='' ? 0 : parseInt($("#descuentoActual").val());
		var descRezago = $("#descuentoRezagos").val()=='' ? 0 : parseInt($("#descuentoRezagos").val());
		actual = actual - (actual * (descActual/100));
		rezago = rezago - (rezago * (descRezago/100));
		var total = actual+rezago;
		$("#total").val(total);
	});

	$("#montoRecibido").blur(function(){
		//alert("Está funcionando esta madre");
		var recibido = parseInt($("#montoRecibido").val());
		var total = parseInt($("#total").val());
		if (recibido<total) {
			alert("Se recibio una cantidad menor al total");
		}else{
			var cambio = recibido-total;
		}
		$("#cambio").val(cambio);
	});

});