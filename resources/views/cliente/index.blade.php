@extends('adminlte::layouts.app')
@section('main-content')
  <div class="container">
      <div class="row">
          <div class="col-md-10">
              <div class="panel panel-default">
                  <div class="panel-heading">Conceptos</div>
                  <div class="panel-body">
                      <a href="{{ url('/cliente/create') }}" class="btn btn-success btn-sm" title="Registrar nuevo concepto">
                          <i class="fa fa-plus" aria-hidden="true"></i> Registrar Nuevo
                      </a>

                      {!! Form::open(['method' => 'GET', 'url' => '/cliente', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                      <div class="input-group">
                          <input type="text" class="form-control" name="search" placeholder="Buscar...">
                          <span class="input-group-btn">
                              <button class="btn btn-default" type="submit">
                                  <i class="fa fa-search"></i>
                              </button>
                          </span>
                      </div>
                      {!! Form::close() !!}

                      <br/>
                      <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Nombre</th><th>Telefono</th><th>Correo</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($cliente as $item)
                                    <tr>
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->nombre }}</td><td>{{ $item->telefono }}</td><td>{{ $item->correo }}</td>
                                        <td>
                                            <a href="{{ url('/cliente/' . $item->id) }}" title="View Cliente"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/cliente/' . $item->id . '/edit') }}" title="Edit Cliente"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/cliente' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Cliente" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $cliente->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
