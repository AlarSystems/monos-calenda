@extends('adminlte::layouts.app')
@section('main-content')
  <div class="container">
      <div class="row">
          <div class="col-md-9">
              <div class="panel panel-default">
                  <div class="panel-heading">Registar nuevo concepto</div>
                  <div class="panel-body">
                      <a href="{{ url('/paquete') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
                      <br />
                      <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/paquete') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('paquete.form')

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
