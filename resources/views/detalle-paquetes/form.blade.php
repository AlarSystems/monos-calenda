<div class="form-group {{ $errors->has('articulo') ? 'has-error' : ''}}">
    <label for="articulo" class="col-md-4 control-label">{{ 'Articulo' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="articulo" type="text" id="articulo" value="{{ $detallepaquete->articulo or ''}}" required>
        {!! $errors->first('articulo', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('cantidad') ? 'has-error' : ''}}">
    <label for="cantidad" class="col-md-4 control-label">{{ 'Cantidad' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="cantidad" type="number" id="cantidad" value="{{ $detallepaquete->cantidad or ''}}" required>
        {!! $errors->first('cantidad', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('id_paquete') ? 'has-error' : ''}}">
    <label for="id_paquete" class="col-md-4 control-label">{{ 'Id Paquete' }}</label>
    <div class="col-md-6">
        {!! Form::select('id_paquete', $paquetes, null, ['class' => 'form-control','placeholder' => 'Seleccione un paquetes']); !!}
        {!! $errors->first('id_paquete', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>
