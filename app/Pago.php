<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pagos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id_evento', 'id_user', 'total', 'fecha'];

    public function eventos()
    {
        return $this->belongsTo('App\Evento');
    }
    public function users()
    {
        return $this->belongsTo('App\User');
    }
    
}
