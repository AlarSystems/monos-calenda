<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DetallePaquete;
use Illuminate\Http\Request;
use App\Paquete;

class DetallePaquetesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $detallepaquetes = DetallePaquete::where('articulo', 'LIKE', "%$keyword%")
                ->orWhere('cantidad', 'LIKE', "%$keyword%")
                ->orWhere('id_paquete', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $detallepaquetes = DetallePaquete::latest()->paginate($perPage);
        }

        return view('detalle-paquetes.index', compact('detallepaquetes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
      $paquetes = Paquete::pluck('nombre', 'id')->all();
        return view('detalle-paquetes.create', compact('paquetes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'id_paquete' => 'required',
			'articulo' => 'required',
			'cantidad' => 'required'
		]);
        $requestData = $request->all();

        DetallePaquete::create($requestData);

        return redirect('detalle-paquetes')->with('flash_message', 'DetallePaquete added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $detallepaquete = DetallePaquete::findOrFail($id);

        return view('detalle-paquetes.show', compact('detallepaquete'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $detallepaquete = DetallePaquete::findOrFail($id);

        return view('detalle-paquetes.edit', compact('detallepaquete'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'id_paquete' => 'required',
			'articulo' => 'required',
			'cantidad' => 'required'
		]);
        $requestData = $request->all();

        $detallepaquete = DetallePaquete::findOrFail($id);
        $detallepaquete->update($requestData);

        return redirect('detalle-paquetes')->with('flash_message', 'DetallePaquete updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DetallePaquete::destroy($id);

        return redirect('detalle-paquetes')->with('flash_message', 'DetallePaquete deleted!');
    }
}
