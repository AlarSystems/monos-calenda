<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Pago;
use Illuminate\Http\Request;
use App\User;
use App\Evento;

class PagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $pago = Pago::where('id_evento', 'LIKE', "%$keyword%")
                ->orWhere('id_user', 'LIKE', "%$keyword%")
                ->orWhere('total', 'LIKE', "%$keyword%")
                ->orWhere('fecha', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $pago = Pago::latest()->paginate($perPage);
        }

        return view('pago.index', compact('pago'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $eventos = Evento::pluck('nombre', 'id')->all();
        $users = User::pluck('name', 'id')->all();
        return view('pago.create', compact('eventos', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'id_evento' => 'required',
			'fecha' => 'required|date',
			'total' => 'required',
			'id_user' => 'required'
		]);
        $requestData = $request->all();

        Pago::create($requestData);

        return redirect('pago')->with('flash_message', 'Pago added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $pago = Pago::findOrFail($id);

        return view('pago.show', compact('pago'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $pago = Pago::findOrFail($id);

        return view('pago.edit', compact('pago'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'id_evento' => 'required',
			'fecha' => 'required|date',
			'total' => 'required',
			'id_user' => 'required'
		]);
        $requestData = $request->all();

        $pago = Pago::findOrFail($id);
        $pago->update($requestData);

        return redirect('pago')->with('flash_message', 'Pago updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Pago::destroy($id);

        return redirect('pago')->with('flash_message', 'Pago deleted!');
    }
}
