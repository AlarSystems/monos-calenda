<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'eventos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['fecha', 'id_user', 'id_cliente', 'id_paquete', 'hora_inicio', 'hora_fin', 'nombre'];

    public function paquetes()
    {
        return $this->belongsTo('App\Paquete');
    }
    public function clientes()
    {
        return $this->belongsTo('App\Cliente');
    }
    public function users()
    {
        return $this->belongsTo('App\User');
    }

}
