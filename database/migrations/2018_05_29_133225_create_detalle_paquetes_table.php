<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetallePaquetesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_paquetes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('articulo')->nullable();
            $table->integer('cantidad')->nullable();
            $table->integer('id_paquete')->nullable()->unsigned();
            $table->timestamps();
        });

        Schema::table('detalle_paquetes', function($table) {
            $table->foreign('id_paquete')->references('id')->on('paquetes')->onDelete('cascade')->onUpdate('cascade');
		    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle_paquetes');
    }
}
